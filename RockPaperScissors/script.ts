import { Tournament } from "./lib/Tournament";

const inputPath:string = "/opt/advent/RockPaperScissors/lib/input.txt"

const tournament:Tournament = Tournament.with(inputPath)

console.log(tournament.calculateTotalScore()) //11386

const decryptedTournament:Tournament = Tournament.withEncrypted(inputPath)

console.log(decryptedTournament.calculateTotalScore()) //13600
