import { Annotations } from './Tournament/Annotations';
import { Games } from './Tournament/Games';
import * as FileSystem from 'fs';

export class Tournament {
  static with(planPath:string):Tournament {
    const rawAnnotations:string = FileSystem.readFileSync(planPath,'utf8')
    const annotations:Annotations = Annotations.in(rawAnnotations)
    const games:Games = Games.from(annotations)

    return new Tournament(games)
  }

  static withEncrypted(planPath:string):Tournament {
    const rawAnnotations:string = FileSystem.readFileSync(planPath,'utf8')
    const annotations:Annotations = Annotations.in(rawAnnotations)
    annotations.decrypt()
    const games:Games = Games.from(annotations)

    return new Tournament(games)
  }

  private games:Games

  constructor(games:Games) {
    this.games = games
  }

  calculateTotalScore():number {
    return this.games.totalScoreForMe()
  }
}
