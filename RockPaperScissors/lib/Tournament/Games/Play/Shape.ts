
type ShapeScores = {
  [key: string]: number
}

export class Shape {
  private ROCK_SCORE:number = 1
  private PAPER_SCORE:number = 2
  private SCISSORS_SCORE:number = 3
  private SCORES:ShapeScores = {
    A: this.ROCK_SCORE,
    B: this.PAPER_SCORE,
    C: this.SCISSORS_SCORE,
    X: this.ROCK_SCORE,
    Y: this.PAPER_SCORE,
    Z: this.SCISSORS_SCORE,
  }
  private shapeScore:number

  constructor(annotation:string) {
    this.shapeScore = this.SCORES[annotation] || 0
  }

  score():number {
    return this.shapeScore
  }

  winsVS(otherShape:Shape):boolean {
    const I_WIN:boolean = true

    if (this.isScissors() && otherShape.isPaper()) {
      return I_WIN
    }
    if (this.isPaper() && otherShape.isRock()) {
      return I_WIN
    }
    if (this.isRock() && otherShape.isScissors()) {
      return I_WIN
    }

    const I_LOSE:boolean = false
    return I_LOSE
  }

  protected isScissors():boolean {
    return this.shapeScore === this.SCISSORS_SCORE
  }

  protected isPaper():boolean {
    return this.shapeScore === this.PAPER_SCORE
  }

  protected isRock():boolean {
    return this.shapeScore === this.ROCK_SCORE
  }
}
