import { Shape } from "./Play/Shape"

export class Play {
  private opponent:Shape
  private me:Shape

  constructor(shapes:string) {
    this.opponent = new Shape(shapes[0] || "")
    this.me = new Shape(shapes[2] || "")
  }

  scoreForMe():number {
    const shapeScore:number = this.me.score()
    const outcomeScore:number = this.myOutcomeRoundScore()

    return shapeScore + outcomeScore
  }

  private myOutcomeRoundScore():number {
    const pointsForDraw:number = 3
    let outcomeScore:number = pointsForDraw

    if (this.opponent.winsVS(this.me)) {
      const noPoints:number = 0
      outcomeScore = noPoints
    } else if (this.me.winsVS(this.opponent)) {
      const pointsForWin:number = 6
      outcomeScore = pointsForWin
    }

    return outcomeScore
  }
}
