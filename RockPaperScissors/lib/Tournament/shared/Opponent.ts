
export class Opponent {
  static play(play:string):Opponent {
    return new Opponent(play)
  }

  private ROCK:string = "A"
  private PAPER:string = "B"
  private SCISSORS:string = "C"
  private play:string

  constructor(play:string) {
    this.play = play
  }

  retrievePlay() {
    return this.play
  }

  isPlayingRock():boolean {
    return this.play === this.ROCK
  }

  isPlayingPaper():boolean {
    return this.play === this.PAPER
  }

  isPlayingScissors():boolean {
    return this.play === this.SCISSORS
  }
}
