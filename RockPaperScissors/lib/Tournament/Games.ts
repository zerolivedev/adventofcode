import { Annotations } from "./Annotations"
import { Play } from "./Games/Play"

export class Games {
  static from(annotations:Annotations):Games {
    return new Games(annotations)
  }

  private plays:Array<Play>

  constructor(annotations:Annotations) {
    this.plays = this.parseToPlaysFrom(annotations)
  }

  totalScoreForMe():number {
    let totalScore:number = 0

    this.plays.forEach((play) => {
      totalScore += play.scoreForMe()
    })

    return totalScore
  }

  private parseToPlaysFrom(annotations:Annotations):Array<Play> {
    const plays:Array<Play> = []

    annotations.forEach((rawPlay:string) => {
      const play = new Play(rawPlay)

      plays.push(play)
    })

    return plays
  }
}
