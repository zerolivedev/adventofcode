import { EncryptedPlay } from "./Annotations/EncryptedPlay"
import { Opponent } from "./shared/Opponent"

export class Annotations {
  static in(annotations:string) {
    return new Annotations(annotations)
  }

  private BY_END_OF_LINE:string = "\r\n"
  private MY_ROCK:string = "X"
  private MY_PAPER:string = "Y"
  private MY_SCISSORS:string = "Z"
  private rawPlays:Array<string> = []

  constructor(annotations:string) {
    this.rawPlays = annotations.split(this.BY_END_OF_LINE)
  }

  decrypt():void {
    const decryptedPlays:Array<string> = []

    this.forEach((rawPlay:string) => {
      const opponent = Opponent.play(rawPlay[0] || "")
      let decryptedPlay:string = opponent.retrievePlay() + " "
      const encryptedPlay = EncryptedPlay.with(rawPlay[2] || "")

      if (encryptedPlay.isLose()) {
        if (opponent.isPlayingRock()) { decryptedPlay += this.MY_SCISSORS}
        if (opponent.isPlayingPaper()) { decryptedPlay += this.MY_ROCK}
        if (opponent.isPlayingScissors()) { decryptedPlay += this.MY_PAPER}
      }

      if (encryptedPlay.isDraw()) {
        if (opponent.isPlayingRock()) { decryptedPlay += this.MY_ROCK}
        if (opponent.isPlayingPaper()) { decryptedPlay += this.MY_PAPER}
        if (opponent.isPlayingScissors()) { decryptedPlay += this.MY_SCISSORS}
      }

      if (encryptedPlay.isWin()) {
        if (opponent.isPlayingRock()) { decryptedPlay += this.MY_PAPER}
        if (opponent.isPlayingPaper()) { decryptedPlay += this.MY_SCISSORS}
        if (opponent.isPlayingScissors()) { decryptedPlay += this.MY_ROCK}
      }

      decryptedPlays.push(decryptedPlay)
    })

    this.rawPlays = decryptedPlays
  }

  forEach(callback:any):void {
    this.rawPlays.forEach(rawPlay => {
      if (rawPlay !== "") {
        callback(rawPlay)
      }
    })
  }
}
