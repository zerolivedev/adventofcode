
export class EncryptedPlay {
  static with(raw:string) {
    return new EncryptedPlay(raw)
  }

  private LOSE_CODE:string = "X"
  private DRAW_CODE:string = "Y"
  private WIN_CODE:string = "Z"
  private value:string

  constructor(raw:string) {
    this.value = raw
  }

  isLose():boolean {
    return this.value === this.LOSE_CODE
  }

  isDraw():boolean {
    return this.value === this.DRAW_CODE
  }

  isWin():boolean {
    return this.value === this.WIN_CODE
  }
}
