import { Annotations } from "../../lib/Tournament/Annotations"
import { expect } from "chai"

describe("Annotations", () => {
  it("can iterate every play", () => {
    const rawAnnotations:string = "A Y\r\nB X\r\nC Z\r\nC X\r\n"
    const annotations:Annotations = Annotations.in(rawAnnotations)
    const rawPlays:Array<string> = []

    annotations.forEach((play:string) => {
      rawPlays.push(play)
    })

    expect(rawPlays.length).to.eq(4)
    expect(rawPlays[0]).to.eq("A Y")
    expect(rawPlays[1]).to.eq("B X")
    expect(rawPlays[2]).to.eq("C Z")
    expect(rawPlays[3]).to.eq("C X")
  })

  it("decrypts plays", () => {
    const rawAnnotations:string = "A Y\r\nB X\r\nB Z\r\nC X\r\n"
    const annotations:Annotations = Annotations.in(rawAnnotations)
    const rawPlays:Array<string> = []

    annotations.decrypt()

    annotations.forEach((play:string) => { rawPlays.push(play) })
    expect(rawPlays.length).to.eq(4)
    expect(rawPlays[0]).to.eq("A X")
    expect(rawPlays[1]).to.eq("B X")
    expect(rawPlays[2]).to.eq("B Z")
    expect(rawPlays[3]).to.eq("C Y")
  })

  it("decrypts other plays", () => {
    const rawAnnotations:string = "A Y\r\nB X\r\nC Z\r\nC X\r\n"
    const annotations:Annotations = Annotations.in(rawAnnotations)
    const rawPlays:Array<string> = []

    annotations.decrypt()

    annotations.forEach((play:string) => { rawPlays.push(play) })
    expect(rawPlays.length).to.eq(4)
    expect(rawPlays[0]).to.eq("A X")
    expect(rawPlays[1]).to.eq("B X")
    expect(rawPlays[2]).to.eq("C X")
    expect(rawPlays[3]).to.eq("C Y")
  })
})
