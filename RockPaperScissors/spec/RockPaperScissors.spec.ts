import { Tournament } from "../lib/Tournament"
import { expect } from "chai"

describe("Rock Paper Scissors", () => {
  it("calculates your total score for a plan", () => {
    const planPath:string = "/opt/advent/RockPaperScissors/spec/input.txt"
    const tournament:Tournament = Tournament.with(planPath)

    const totalScore:number = tournament.calculateTotalScore()

    expect(totalScore).to.eq(22)
  })

  it("Following the Elf's instructions for the second column", () => {
    const planPath:string = "/opt/advent/RockPaperScissors/spec/input.txt"
    const tournament:Tournament = Tournament.withEncrypted(planPath)

    const totalScore:number = tournament.calculateTotalScore()

    expect(totalScore).to.eq(14)
  })
})
