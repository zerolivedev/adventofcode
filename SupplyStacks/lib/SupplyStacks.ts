import { linesToStacks } from "./SupplyStacks/linesToStacks"
import { Command } from "./SupplyStacks/Command"
import { Stack } from "./SupplyStacks/Stack"
import { File } from "../../File"

export class SupplyStacks {
  static from(stacksPath:string):SupplyStacks {
    const lines:Array<string> = File.from(stacksPath).toLines()
    const stacks:Array<Stack> = linesToStacks(lines)

    return new SupplyStacks(stacks)
  }

  private DEFAULT_MOVER = "CrateMover 9000"
  private stacks:Array<Stack>
  private commands:Array<Command> = []

  constructor(stacks:Array<Stack>) {
    this.stacks = stacks
  }

  reorganizeFollowingSequenceIn(commandsPath:string, mover:string=this.DEFAULT_MOVER):void {
    this.commands = this.fillCommandsWith(commandsPath)

    this.commands.forEach((command:Command) => {
      const outcomeStack:Stack = this.stacks.find((stack:Stack) => { return stack.is(command.outcomeStackName())}) || Stack.asNull()
      const incomingStack:Stack = this.stacks.find((stack:Stack) => { return stack.is(command.incomingStackName())}) || Stack.asNull()

      const crates:Array<string> = incomingStack.extract(command.quantityOfCratesToExtract())

      if (mover !== this.DEFAULT_MOVER) {
        outcomeStack.addInBatch(crates)
      } else {
        outcomeStack.add(crates)
      }
    })
  }

  cratesEndUpAsString():string {
    let upperCrates:string = ""

    this.stacks.forEach((stack:Stack) => {
      upperCrates += stack.upperCrate()
    })

    return upperCrates
  }

  private fillCommandsWith(commandsPath:string):Array<Command> {
    const lines:Array<string> = File.from(commandsPath).toLines()
    const commands:Array<Command> = []

    lines.forEach((line:string) => {
      commands.push(Command.from(line))
    })

    return commands
  }
}
