
export class Command {
  static from(raw:string):Command {
    const quantity:number = Number(raw.split(" ")[1])
    const incomingStack:string = raw.split(" ")[3] || ""
    const outcomeStack:string = raw.split(" ")[5] || ""

    return new Command(quantity, incomingStack, outcomeStack)
  }

  private quantity:number
  private incomingStack:string
  private outcomeStack:string

  constructor(quantity:number, incomingStack:string, outcomeStack:string) {
    this.quantity = quantity
    this.incomingStack = incomingStack
    this.outcomeStack = outcomeStack
  }

  quantityOfCratesToExtract():number {
    return this.quantity
  }

  incomingStackName():string {
    return this.incomingStack
  }

  outcomeStackName():string {
    return this.outcomeStack
  }
}
