
export class Stack {
  static asNull():Stack {
    return new Stack('', [])
  }

  static with(rawStack:Array<string>):Stack {
    const stackName:string = rawStack.pop() || ""
    const crates:Array<string> = rawStack.filter((line:string) => { return (line !== " " && line !== "") })

    return new Stack(stackName, crates)
  }

  private stackName:string
  private crates:Array<string>

  constructor(stackName:string, crates:Array<string>) {
    this.stackName = stackName
    this.crates = crates
  }

  extract(quantity:number):Array<string> {
    const outcomeCrates:Array<string> = this.crates.splice(0, quantity)

    return outcomeCrates
  }

  add(incomingCrates:Array<string>):void {
    incomingCrates.forEach((crate:string) => {
      this.crates.unshift(crate)
    })
  }

  addInBatch(incomingCrates:Array<string>):void {
    this.crates = [...incomingCrates, ...this.crates]
  }

  upperCrate():string {
    return this.crates[0] || ""
  }

  is(name:string):boolean {
    return (this.stackName === name)
  }
}
