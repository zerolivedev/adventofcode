import { Stack } from "./Stack"

export function linesToStacks(lines:Array<string>):Array<Stack> {
  const sanitizedLines:Array<string> = lines.map((line:string) => {
    return line.replace(/\[|\]/g, " ")
  })

  const columns:Array<Array<string>> = []
  const columnsNumbers:string = sanitizedLines[sanitizedLines.length - 1] || "0"
  const columnsQuantity:number = Number(columnsNumbers[columnsNumbers.length - 1])

  sanitizedLines.forEach((line:string) => {
    for(let currentColumn = 1; currentColumn <= columnsQuantity; currentColumn++) {
      if (!columns[currentColumn]) {
        columns[currentColumn] = []
      }

      let lineColumn:number = currentColumn
      if (currentColumn !== 1) {
        lineColumn += 3 * (currentColumn - 1)
      }

      columns[currentColumn]?.push(line.substr(lineColumn, 1))
    }
  })

  columns.shift()
  return columns.map((column:Array<string>) => { return Stack.with(column) })
}
