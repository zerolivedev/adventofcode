import { expect } from "chai"
import { Stack } from "../../lib/SupplyStacks/Stack"

describe("Stack", () => {
  const aName:string = "a name"
  const crate:string = "Z"

  it("knows its name", () => {
    const rawStack:Array<string> = [aName]
    const stack:Stack = Stack.with(rawStack)

    const answer:boolean = stack.is(aName)

    expect(answer).to.eq(true)
  })

  it("knows its upper crate", () => {
    const crate:string = "Z"
    const rawStack:Array<string> = [crate, aName]
    const stack:Stack = Stack.with(rawStack)

    const upperCrate:string = stack.upperCrate()

    expect(upperCrate).to.eq(crate)
  })

  it("extracts the upper crates", () => {
    const rawStack:Array<string> = [crate, crate, aName]
    const stack:Stack = Stack.with(rawStack)

    const crates:Array<string> = stack.extract(2)

    expect(crates).to.have.all.members([crate, crate])
  })

  it("adds crates", () => {
    const rawStack:Array<string> = [aName]
    const stack:Stack = Stack.with(rawStack)

    stack.add([crate])

    const upperCrate:string = stack.upperCrate()
    expect(upperCrate).to.eq(crate)
  })

  it("adds crates in batch", () => {
    const anotherCrate:string = "A"
    const rawStack:Array<string> = [aName]
    const stack:Stack = Stack.with(rawStack)

    stack.addInBatch([anotherCrate, crate])

    const upperCrate:string = stack.upperCrate()
    expect(upperCrate).to.eq(anotherCrate)
  })
})
