import { Command } from "../../lib/SupplyStacks/Command"
import { expect } from "chai"

describe("Command", () => {
  it("knows the quantity of crates has to extract", () => {
    const rawCommand:string = "move 3 from 1 to 2"
    const command:Command = Command.from(rawCommand)

    const quantity = command.quantityOfCratesToExtract()

    expect(quantity).to.eq(3)
  })

  it("knows the incoming column", () => {
    const rawCommand:string = "move 3 from 1 to 2"
    const command:Command = Command.from(rawCommand)

    const stackName:string = command.incomingStackName()

    expect(stackName).to.eq("1")
  })

  it("knows the outcome column", () => {
    const rawCommand:string = "move 3 from 1 to 2"
    const command:Command = Command.from(rawCommand)

    const stackName:string = command.outcomeStackName()

    expect(stackName).to.eq("2")
  })
})
