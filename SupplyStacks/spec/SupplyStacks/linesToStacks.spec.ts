import { linesToStacks } from "../../lib/SupplyStacks/linesToStacks"
import { Stack } from "../../lib/SupplyStacks/Stack"
import { File } from "../../../File"
import { expect } from "chai"


describe("linesToStacks", () => {
  it("parses lines to stacks", () => {
    const path:string = "/opt/advent/SupplyStacks/spec/current_organization_test_input.txt"
    const lines:Array<string> = File.from(path).toLines()

    const stacks:Array<Stack> = linesToStacks(lines)

    expect(stacks.length).to.eq(3)
  })
})
