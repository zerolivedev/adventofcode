import { SupplyStacks } from "../lib/SupplyStacks"
import { expect } from "chai"

describe("Supply Stacks", () => {
  const currentOrganizationPath:string = "/opt/advent/SupplyStacks/lib/currentOrganization.txt"
  const reorganizationPlanPath:string = "/opt/advent/SupplyStacks/lib/commands.txt"

  it("retrieves crate ends up on top of each stack", () => {
    const supplyStacks:SupplyStacks = SupplyStacks.from(currentOrganizationPath)
    supplyStacks.reorganizeFollowingSequenceIn(reorganizationPlanPath)

    const cratesEndUp:string = supplyStacks.cratesEndUpAsString()

    expect(cratesEndUp).to.eq("RNZLFZSJH")
  })

  it("retrieves crate ends up on top of each stack with another crate mover", () => {
    const supplyStacks:SupplyStacks = SupplyStacks.from(currentOrganizationPath)
    const anotherCrateMover:string = "CrateMover 9001"
    supplyStacks.reorganizeFollowingSequenceIn(reorganizationPlanPath, anotherCrateMover)

    const cratesEndUp:string = supplyStacks.cratesEndUpAsString()

    expect(cratesEndUp).to.eq("CNSFCGJSM")
  })
})
