import { Devise } from "../lib/Devise"
import { expect } from "chai"

describe("Devise", () => {
  it("test", () => {
    const commandsPath:string = "/opt/advent/NoSpaceLeftOnDevice/spec/test_input.txt"
    const devise:Devise = Devise.turnOn()
    devise.execute(commandsPath)

    const totalSize:number = devise.smallDirectoriesSize()

    expect(totalSize).to.eq(95437)
  })
})
