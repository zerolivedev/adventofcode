
export class DataStream {
  static from(stream:string):DataStream {
    return new DataStream(stream)
  }

  private stream:Array<string>

  constructor(stream:string) {
    this.stream = stream.split("")
  }

  startOfMessageMarker():number {
    let message = 0

    this.stream.forEach((_value:string, position:number) => {
      const sequence:Set<string> = this.buildSequenceMessageStartingAt(position)

      if (this.hasFourteenCharactersThatAreAllDifferent(sequence)) {
        if (!this.isMarkerFound(message)) {
          message = position + 14
        }
      }
    })

    return message
  }

  startOfPackageMarker():number {
    let marker = 0

    this.stream.forEach((_value:string, position:number) => {
      const sequence:Set<string> = this.buildSequenceMarkerStartingAt(position)

      if (this.hasFourCharactersThatAreAllDifferent(sequence)) {
        if (!this.isMarkerFound(marker)) {
          marker = position + 4
        }
      }
    })

    return marker
  }

  private buildSequenceMarkerStartingAt(position:number):Set<string> {
    const sequence:Set<string> = new Set()

    sequence.add(this.stream[position] || "")
    sequence.add(this.stream[position + 1] || "")
    sequence.add(this.stream[position + 2] || "")
    sequence.add(this.stream[position + 3] || "")

    return sequence
  }

  private buildSequenceMessageStartingAt(position:number):Set<string> {
    const sequence:Set<string> = new Set()

    sequence.add(this.stream[position] || "")
    sequence.add(this.stream[position + 1] || "")
    sequence.add(this.stream[position + 2] || "")
    sequence.add(this.stream[position + 3] || "")
    sequence.add(this.stream[position + 4] || "")
    sequence.add(this.stream[position + 5] || "")
    sequence.add(this.stream[position + 6] || "")
    sequence.add(this.stream[position + 7] || "")
    sequence.add(this.stream[position + 8] || "")
    sequence.add(this.stream[position + 9] || "")
    sequence.add(this.stream[position + 10] || "")
    sequence.add(this.stream[position + 11] || "")
    sequence.add(this.stream[position + 12] || "")
    sequence.add(this.stream[position + 13] || "")

    return sequence
  }

  private hasFourCharactersThatAreAllDifferent(sequence:Set<string>):boolean {
    return (sequence.size === 4)
  }

  private hasFourteenCharactersThatAreAllDifferent(sequence:Set<string>):boolean {
    return (sequence.size === 14)
  }

  private isMarkerFound(marker:number):boolean {
    return marker !== 0
  }
}
