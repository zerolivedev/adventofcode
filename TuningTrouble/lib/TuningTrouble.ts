import { DataStream } from "./Datastream"
import { File } from "../../File"

export class TuningTrouble {
  static with(path:string):TuningTrouble {
    return new TuningTrouble(path)
  }

  private dataStreams:Array<DataStream> = []

  constructor(path:string) {
    const lines:Array<string> = File.from(path).toLines()
    lines.forEach((line:string) => {
      const dataStream:DataStream = DataStream.from(line)

      this.dataStreams.push(dataStream)
    })
  }

  countProcessedCharacters():number {
    let count:number = 0

    this.dataStreams.forEach((dataStream:DataStream) => {
      count += dataStream.startOfPackageMarker()
    })

    return count
  }

  countProcessedCharactersUntilMessage():number {
    let count:number = 0

    this.dataStreams.forEach((dataStream:DataStream) => {
      count += dataStream.startOfMessageMarker()
    })

    return count
  }
}
