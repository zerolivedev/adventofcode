import { TuningTrouble } from "../lib/TuningTrouble"
import { expect } from "chai"

describe("Tuning Trouble", () => {
  it("knows how many characters need to be processed before the first start-of-packet marker is detected", () => {
    const inputPath:string = "/opt/advent/TuningTrouble/lib/input.txt"
    const tunningTrouble:TuningTrouble = TuningTrouble.with(inputPath)

    const processedCharacters:number = tunningTrouble.countProcessedCharacters()

    expect(processedCharacters).to.eq(1262)
  })

  it("knows how many characters need to be processed before the first start-of-message marker is detected", () => {
    const inputPath:string = "/opt/advent/TuningTrouble/lib/input.txt"
    const tunningTrouble:TuningTrouble = TuningTrouble.with(inputPath)

    const processedCharacters:number = tunningTrouble.countProcessedCharactersUntilMessage()

    expect(processedCharacters).to.eq(3444)
  })
})
