import { DataStream } from "../lib/Datastream"
import { expect } from "chai"

describe("Data stream", () => {
  it("knows where is the start-of-packet marker", () => {
    const stream:string = "mjqjpqmgbljsphdztnvjfqwrcgsmlb"
    const dataStream:DataStream = DataStream.from(stream)

    const startOfPackageMarker:number = dataStream.startOfPackageMarker()

    expect(startOfPackageMarker).to.eq(7)
  })
  it("knows where is the start-of-message marker", () => {
    const stream:string = "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg"
    const dataStream:DataStream = DataStream.from(stream)

    const startOfMessageMarker:number = dataStream.startOfMessageMarker()

    expect(startOfMessageMarker).to.eq(29)
  })
})
