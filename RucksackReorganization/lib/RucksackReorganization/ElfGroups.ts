import { ElfGroup } from "./ElfGroup"
import { Rucksack } from "./Rucksack"

export class ElfGroups {
  static distribute(rucksacks:Array<Rucksack>):ElfGroups {
    return new ElfGroups(rucksacks)
  }

  private groups:Array<ElfGroup> = []

  constructor(rucksacks:Array<Rucksack>) {
    let currentGroup:ElfGroup = ElfGroup.empty()

    rucksacks.forEach((rucksack:Rucksack) => {
      if (currentGroup.hasQuantity(3)) {
        this.groups.push(currentGroup)
        currentGroup = ElfGroup.empty()
      }

      currentGroup.add(rucksack)
    })
    this.groups.push(currentGroup)
  }

  forEach(callback:Function):void {
    this.groups.forEach((group:ElfGroup) => callback(group))
  }
}
