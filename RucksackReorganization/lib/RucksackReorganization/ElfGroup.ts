import { Rucksack } from "./Rucksack"

export class ElfGroup {
  static empty():ElfGroup {
    return new ElfGroup([])
  }

  static with(ruckSacks:Array<Rucksack>):ElfGroup {
    return new ElfGroup(ruckSacks)
  }

  private firstRuckSacks:Rucksack
  private secondRuckSacks:Rucksack
  private thirdRuckSacks:Rucksack

  constructor(rucksacks:Array<Rucksack>) {
    this.firstRuckSacks = rucksacks[0] || Rucksack.empty()
    this.secondRuckSacks = rucksacks[1] || Rucksack.empty()
    this.thirdRuckSacks = rucksacks[2] || Rucksack.empty()
  }

  add(rucksack:Rucksack):void {
    if (this.firstRuckSacks.isEmpty()) {
      this.firstRuckSacks = rucksack
    } else {
      if (this.secondRuckSacks.isEmpty()) {
        this.secondRuckSacks = rucksack
      } else {
        this.thirdRuckSacks = rucksack
      }
    }
  }

  badge():string {
    const repeatedItems:Array<string> = []

    this.firstRuckSacks.forEach((item:string) => {
      if (this.secondRuckSacks.includes(item)) {
        repeatedItems.push(item)
      }
    })

    let badge:string = ""
    this.thirdRuckSacks.forEach((item:string) => {
      if (repeatedItems.includes(item)) {
        badge = item
      }
    })

    return badge
  }

  hasQuantity(quantity:number):boolean {
    let count = 0

    if(!this.firstRuckSacks.isEmpty()) {
      count += 1
    }
    if(!this.secondRuckSacks.isEmpty()) {
      count += 1
    }
    if(!this.thirdRuckSacks.isEmpty()) {
      count += 1
    }

    return (count === quantity)
  }
}
