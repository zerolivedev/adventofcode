
export class Rucksack {
  static empty():Rucksack {
    return new Rucksack("")
  }

  static filledWith(items:string):Rucksack {
    return new Rucksack(items)
  }

  private firstCompartment:Array<string> = []
  private secondCompartment:Array<string> = []

  constructor(items:string) {
    this.fillCompartmentsWith(items)
  }

  findRepeatedItem():string {
    const repeatedItem:string = this.firstCompartment.find(item => {
      return this.secondCompartment.includes(item)
    }) || ''

    return repeatedItem
  }

  forEach(callback:Function):void {
    this.firstCompartment.forEach(item => callback(item))
    this.secondCompartment.forEach(item => callback(item))
  }

  includes(item:string):boolean {
    return this.firstCompartment.includes(item) || this.secondCompartment.includes(item)
  }

  isEmpty():boolean {
    return this.firstCompartment.length === 0 && this.secondCompartment.length === 0
  }

  private fillCompartmentsWith(items:string):void {
    const middleIndex = (items.length / 2)

    items.split("").forEach((item, index) => {
      if (index < middleIndex) {
        this.firstCompartment.push(item)
      } else {
        this.secondCompartment.push(item)
      }
    })
  }
}
