
export class Evaluator {
  private static LETTERS_IN_ALPHABET = 26

  static appreciate(item:string):number {
    let value:number = this.calculateValueForLowerCase(item)

    if (this.isUpperCase(item)) {
      value = (value + this.LETTERS_IN_ALPHABET)
    }

    return value
  }

  private static calculateValueForLowerCase(item:string):number {
    return item.toLowerCase().charCodeAt(0) - 97 + 1
  }

  private static isUpperCase(letter:string):boolean {
    return (letter.toUpperCase() === letter)
  }
}
