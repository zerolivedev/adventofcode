import { Evaluator } from "./RucksackReorganization/Evaluator"
import { ElfGroups } from "./RucksackReorganization/ElfGroups"
import { ElfGroup } from "./RucksackReorganization/ElfGroup"
import { Rucksack } from "./RucksackReorganization/Rucksack"

import * as FileSystem from "fs"

export class RucksackReorganization {
  static with(rucksackPath:string):RucksackReorganization {
    return new RucksackReorganization(rucksackPath)
  }

  private rucksacks:Array<Rucksack> = []

  constructor(rucksackPath:string) {
    const rawRucksacks = FileSystem.readFileSync(rucksackPath,'utf8')

    rawRucksacks.split("\r\n").forEach(rawRucksack => {
      if (rawRucksack !== "") {
        const rucksack:Rucksack = Rucksack.filledWith(rawRucksack)

        this.rucksacks.push(rucksack)
      }
    })
  }

  sumPriorities():number {
    return this.rucksacks.reduce((total, rucksack) => {
      const item:string = rucksack.findRepeatedItem()
      const itemValue:number = Evaluator.appreciate(item)

      return total + itemValue
    }, 0)
  }

  sumBadgesPriority():number {
    const groups:ElfGroups = ElfGroups.distribute(this.rucksacks)
    let badgesPriority:number = 0

    groups.forEach((group:ElfGroup) => {
      const badge:string = group.badge()

      badgesPriority += Evaluator.appreciate(badge)
    })

    return badgesPriority
  }
}
