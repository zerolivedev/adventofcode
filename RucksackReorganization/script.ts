import { RucksackReorganization } from "./lib/RucksackReorganization"

const path:string = "/opt/advent/RucksackReorganization/lib/input.txt"
const rucksackReorganization:RucksackReorganization = RucksackReorganization.with(path)

let result:number = rucksackReorganization.sumPriorities()
console.log(result); //7766

result = rucksackReorganization.sumBadgesPriority()
console.log(result); //2415
