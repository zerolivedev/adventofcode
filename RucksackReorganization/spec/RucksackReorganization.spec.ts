import { RucksackReorganization } from "../lib/RucksackReorganization"
import { expect } from "chai"

describe("Rucksack Reorganization", () => {
  const rucksackPath:string = "/opt/advent/RucksackReorganization/spec/input.txt"

  it("sum of the priorities of those item types", () => {
    const rucksackReorganization:RucksackReorganization = RucksackReorganization.with(rucksackPath)

    const prioritySum:number = rucksackReorganization.sumPriorities()

    expect(prioritySum).to.eq(157)
  })

  it("sums the item priority that corresponds to the badges of each three-Elf group", () => {
    const rucksackReorganization:RucksackReorganization = RucksackReorganization.with(rucksackPath)

    const totalBadgesValue:number = rucksackReorganization.sumBadgesPriority()

    expect(totalBadgesValue).to.eq(70)
  })
})
