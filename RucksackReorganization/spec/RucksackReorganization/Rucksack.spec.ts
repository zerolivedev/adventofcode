import { Rucksack } from "../../lib/RucksackReorganization/Rucksack"
import { expect } from "chai"

describe("Rucksack", () => {
  it("finds the repeated item", () => {
    const rucksack:Rucksack = Rucksack.filledWith(someItem + someItem)

    const repeatedItem:string = rucksack.findRepeatedItem()

    expect(repeatedItem).to.eq(someItem)
  })

  const someItem:string = "a"
})
