import { ElfGroups } from "../../lib/RucksackReorganization/ElfGroups"
import { ElfGroup } from "../../lib/RucksackReorganization/ElfGroup"
import { Rucksack } from "../../lib/RucksackReorganization/Rucksack"
import { expect } from "chai"

describe("ElfGroups", () => {
  it("executes a callback for each group", () => {
    const rucksacks:Array<Rucksack> = [
      Rucksack.filledWith("vJrwpWtwJgWrhcsFMMfFFhFp"),
      Rucksack.filledWith("jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL"),
      Rucksack.filledWith("PmmdzqPrVvPwwTWBwg"),
      Rucksack.filledWith("wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn"),
      Rucksack.filledWith("ttgJtRGJQctTZtZT"),
      Rucksack.filledWith("CrZsJsPPZsGzwwsLwLmpwMDw")
    ]
    const elfGroups:ElfGroups = ElfGroups.distribute(rucksacks)
    let groupsQuantity:number = 0

    elfGroups.forEach((_group:ElfGroup) => {
      groupsQuantity += 1
    })

    expect(groupsQuantity).to.eq(2)
  })
})
