import { Evaluator } from "../../lib/RucksackReorganization/Evaluator"
import { expect } from "chai"

describe("Evaluator", () => {
  it("appreciate item's value", () => {
    const itemWithValueOfOne:string = 'a'

    const value:number = Evaluator.appreciate(itemWithValueOfOne)

    expect(value).to.eq(1)
  })

  it("appreciate item's value", () => {
    const item:string = 'Z'

    const value:number = Evaluator.appreciate(item)

    expect(value).to.eq(52)
  })
})
