import { ElfGroup } from "../../lib/RucksackReorganization/ElfGroup"
import { Rucksack } from "../../lib/RucksackReorganization/Rucksack"
import { expect } from "chai"

describe("ElfGroup", () => {
  it("retrieves the groups' badge", () => {
    const aRucksack:Rucksack = Rucksack.filledWith("vJrwpWtwJgWrhcsFMMfFFhFp")
    const anotherRucksack:Rucksack = Rucksack.filledWith("jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL")
    const anotherMoreRucksack:Rucksack = Rucksack.filledWith("PmmdzqPrVvPwwTWBwg")
    const ruckSacks:Array<Rucksack> = [aRucksack, anotherRucksack, anotherMoreRucksack]
    const elfGroup:ElfGroup = ElfGroup.with(ruckSacks)

    const badge:string = elfGroup.badge()

    expect(badge).to.eq("r")
  })

  it("retrieves the other groups' badge", () => {
    const aRucksack:Rucksack = Rucksack.filledWith("wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn")
    const anotherRucksack:Rucksack = Rucksack.filledWith("ttgJtRGJQctTZtZT")
    const anotherMoreRucksack:Rucksack = Rucksack.filledWith("CrZsJsPPZsGzwwsLwLmpwMDw")
    const ruckSacks:Array<Rucksack> = [aRucksack, anotherRucksack, anotherMoreRucksack]
    const elfGroup:ElfGroup = ElfGroup.with(ruckSacks)

    const badge:string = elfGroup.badge()

    expect(badge).to.eq("Z")
  })
})
