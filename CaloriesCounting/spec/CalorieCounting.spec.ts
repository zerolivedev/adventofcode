import { CaloriesCounting } from "../lib/CaloriesCounting"
import { expect } from "chai"

describe("CaloriesCounting", () => {
  const filePath:string = "/opt/advent/CaloriesCounting/spec/test_input.txt"

  it("Find the Elf carrying the most Calories", () => {
    const caloriesCounting:CaloriesCounting = new CaloriesCounting(filePath)

    const calories:number = caloriesCounting.findMostCalories()

    expect(calories).to.eq(24000)
  })

  it("Find the top three Elves carrying the most Calories", () => {
    const caloriesCounting:CaloriesCounting = new CaloriesCounting(filePath)

    const calories:number = caloriesCounting.totalFromTopThreeCalories()

    expect(calories).to.eq(45000)
  })
})
