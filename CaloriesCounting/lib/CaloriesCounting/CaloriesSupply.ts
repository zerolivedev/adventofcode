
export class CaloriesSupply {
  private calories:Array<string> = []

  static empty():CaloriesSupply {
    return new CaloriesSupply()
  }

  annotate(calories:string):void {
    this.calories.push(calories)
  }

  total():number {
    return this.calories.reduce((total, calories) => {
      return total + Number(calories)
    }, 0)
  }
}
