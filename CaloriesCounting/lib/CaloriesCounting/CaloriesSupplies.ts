import { CaloriesSupply } from "./CaloriesSupply"

export class CaloriesSupplies {
  private supplies:Array<CaloriesSupply> = []

  add(supply:CaloriesSupply) {
    this.supplies.push(supply)
  }

  retrieveTopThree():CaloriesSupplies {
    const suppliesOrderedByCalories = this.supplies.sort((a,b) => { return b.total() - a.total() })
    const topThree = new CaloriesSupplies()

    topThree.add(suppliesOrderedByCalories[0] || CaloriesSupply.empty())
    topThree.add(suppliesOrderedByCalories[1] || CaloriesSupply.empty())
    topThree.add(suppliesOrderedByCalories[2] || CaloriesSupply.empty())

    return topThree
  }

  findMostCalories():CaloriesSupply {
    let mostCalories = this.supplies[0] || CaloriesSupply.empty()

    this.supplies.forEach(supply => {
      if (supply.total() > mostCalories.total()) {
        mostCalories = supply
      }
    })

    return mostCalories
  }

  total():number {
    const totalCalories:number = this.supplies.reduce((total, supply) => {
      return total += supply.total()
    }, 0)

    return totalCalories
  }
}
