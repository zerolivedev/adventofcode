import { CaloriesSupplies } from "./CaloriesCounting/CaloriesSupplies"
import { CaloriesSupply } from "./CaloriesCounting/CaloriesSupply"
import * as FileSystem from 'fs';

export class CaloriesCounting {
  private caloriesSupplies:CaloriesSupplies
  constructor(filePath:string) {
    this.caloriesSupplies = this.buildSuppliesFrom(filePath)
  }

  findMostCalories():number {
    const supply:CaloriesSupply = this.caloriesSupplies.findMostCalories()

    return supply.total()
  }

  totalFromTopThreeCalories():number {
    const topThree:CaloriesSupplies = this.caloriesSupplies.retrieveTopThree()

    return topThree.total()
  }

  private buildSuppliesFrom(filePath:string):CaloriesSupplies {
    const fileContent:string = FileSystem.readFileSync(filePath,'utf8')
    const caloriesList:Array<string> = fileContent.trim().split("\r\n")
    const caloriesSupplies:CaloriesSupplies = new CaloriesSupplies()

    let currentSupply:CaloriesSupply = new CaloriesSupply()
    caloriesList.forEach((annotation) => {
      if(this.isEndOfList(annotation)) {
        caloriesSupplies.add(currentSupply)
        currentSupply = new CaloriesSupply()
      } else {
        currentSupply.annotate(annotation)
      }
    })
    caloriesSupplies.add(currentSupply)

    return caloriesSupplies
  }

  private isEndOfList(annotation:string):boolean {
    return (annotation === "")
  }
}
