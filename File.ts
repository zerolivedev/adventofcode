import * as FileSystem from 'fs';

export class File {
  static from(path:string):File {
    return new File(path)
  }

  private fileContent:string

  constructor(path:string) {
    this.fileContent = FileSystem.readFileSync(path,'utf8')
  }

  toLines():Array<string> {
    const lines:Array<string> = this.fileContent.split("\r\n")
    if (lines[lines.length - 1] === "") {
      lines.pop()
    }

    return lines
  }
}
