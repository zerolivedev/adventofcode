import { CaloriesCounting } from "../CaloriesCounting/lib/CaloriesCounting"

const filePath:string = "/opt/advent/challenges/inputs/first.txt"

const calories:number = new CaloriesCounting(filePath).findMostCalories()

console.log(calories) //68775
