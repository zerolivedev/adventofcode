import { Range } from "./ElfPair/Range"

export class ElfPair {
  private static FIRST:number = 0
  private static SECOND:number = 1
  private static RANGES_SEPARATOR:string = ","

  static with(plan:string):ElfPair {
    const firstPlan:Array<number> = this.createRangeWith(plan, this.FIRST)
    const secondPlan:Array<number> = this.createRangeWith(plan, this.SECOND)

    return new ElfPair(firstPlan, secondPlan)
  }

  private firstPlan:Array<number>
  private secondPlan:Array<number>

  constructor(firstPlan:Array<number>, secondPlan:Array<number>) {
    this.firstPlan = firstPlan
    this.secondPlan = secondPlan
  }

  isEveryOverlappingSections():boolean {
    return this.overlapSecondSectionsInFirst() || this.overlapFirstSectionsInSecond()
  }

  isAnyOverlappingSections() {
    return this.firstPlan.some((sectionId:number) => {
      return this.secondPlan.includes(sectionId)
    }) || this.secondPlan.some((sectionId:number) => {
      return this.firstPlan.includes(sectionId)
    })
  }

  private static createRangeWith(plan:string, position:number) {
    const rawPlan:string = plan.split(this.RANGES_SEPARATOR)[position] || ""
    const rangePlan:Array<number> = Range.createWith(rawPlan)

    return rangePlan
  }

  private overlapSecondSectionsInFirst():boolean {
    return this.everySectionIsOverlapIn(this.secondPlan, this.firstPlan)
  }

  private overlapFirstSectionsInSecond():boolean {
    return this.everySectionIsOverlapIn(this.firstPlan, this.secondPlan)
  }

  private everySectionIsOverlapIn(aPlan:Array<number>, anotherPlan:Array<number>):boolean {
    return aPlan.every((sectionId:number) => {
      return anotherPlan.includes(sectionId)
    })
  }
}
