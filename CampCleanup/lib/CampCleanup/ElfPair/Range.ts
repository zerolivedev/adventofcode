
export class Range {
  static createWith(raw:string):Array<number> {
    const rawStart:string = raw.split("-")[0] || "0"
    const rawEnd:string = raw.split("-")[1] || "0"
    const start:number = parseInt(rawStart)
    const end:number = parseInt(rawEnd)

    return Array.from(Array(end - start + 1).keys(), x => x + start);
  }
}
