import { ElfPair } from "./CampCleanup/ElfPair"
import { File } from "../../File"

export class CampCleanup {
  static withPlan(path:string):CampCleanup {
    const rawPairs:Array<string> = File.from(path).toLines()
    const pairs:Array<ElfPair> = this.buildPairsWith(rawPairs)
    return new CampCleanup(pairs)
  }

  private pairs:Array<ElfPair>

  constructor(pairs:Array<ElfPair>) {
    this.pairs = pairs
  }

  countOverlappedSections():number {
    const count:number = this.pairs.reduce((total:number, pair:ElfPair) => {
      if (pair.isEveryOverlappingSections()) {
        total += 1
      }

      return total
    }, 0)

    return count
  }

  countAnyOverlappedSections():number {
    const count:number = this.pairs.reduce((total:number, pair:ElfPair) => {
      if (pair.isAnyOverlappingSections()) {
        total += 1
      }

      return total
    }, 0)

    return count
  }

  private static buildPairsWith(rawPairs:Array<string>):Array<ElfPair> {
    return rawPairs.map((raw:string) => ElfPair.with(raw))
  }
}
