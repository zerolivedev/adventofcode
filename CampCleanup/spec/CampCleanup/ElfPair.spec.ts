import { ElfPair } from "../../lib/CampCleanup/ElfPair"
import { expect } from "chai"

describe("ElfPair", () => {
  it("knows if sections does not overlap", () => {
    const plan:string = "1-2,4-5"
    const elfPair:ElfPair = ElfPair.with(plan)

    const isOverlapping:boolean = elfPair.isEveryOverlappingSections()

    expect(isOverlapping).to.eq(false)
  })

  it("knows if sections overlap", () => {
    const plan:string = "1-6,4-5"
    const elfPair:ElfPair = ElfPair.with(plan)

    const isOverlapping:boolean = elfPair.isEveryOverlappingSections()

    expect(isOverlapping).to.eq(true)
  })

  it("knows if sections overlap", () => {
    const plan:string = "4-5,1-6"
    const elfPair:ElfPair = ElfPair.with(plan)

    const isOverlapping:boolean = elfPair.isEveryOverlappingSections()

    expect(isOverlapping).to.eq(true)
  })
})
