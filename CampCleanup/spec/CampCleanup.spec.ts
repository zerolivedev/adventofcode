import { CampCleanup } from "../lib/CampCleanup"
import { expect } from "chai"

describe("Camp Cleanup", () => {
  const path:string = "/opt/advent/CampCleanup/spec/test_input.txt"

  it("counts how many assignment pairs does one range fully contain the other", () => {

    const count:number = CampCleanup.withPlan(path).countOverlappedSections()

    expect(count).to.eq(2)
  })

  it("counts how many assignment pairs do the ranges overlap", () => {

    const count:number = CampCleanup.withPlan(path).countAnyOverlappedSections()

    expect(count).to.eq(4)
  })
})
