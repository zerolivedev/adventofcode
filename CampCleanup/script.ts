import { CampCleanup } from "./lib/CampCleanup"

const path:string = "/opt/advent/CampCleanup/lib/input.txt"

let result:number = CampCleanup.withPlan(path).countOverlappedSections()
console.log(result) //494

result = CampCleanup.withPlan(path).countAnyOverlappedSections()
console.log(result) //833
