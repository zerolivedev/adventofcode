FROM node:16.17.0-slim

WORKDIR /opt/advent

COPY package* .

RUN npm install

COPY . .
